# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/fonts-firge/issues
# Bug-Submit: https://github.com/<user>/fonts-firge/issues/new
# Changelog: https://github.com/<user>/fonts-firge/blob/master/CHANGES
# Documentation: https://github.com/<user>/fonts-firge/wiki
Repository-Browse: https://github.com/yuru7/Firge
Repository: https://github.com/yuru7/Firge
